- Could not connect webrtc from LAN using chrome, it works only for firefox
  https://github.com/peers/peerjs/issues/608

- Tried to create mirror effect but on the fullscreen does not work (fixed by using canvas)
  video::-webkit-media-controls {
  display: none !important;
  }

            ::-webkit-media-controls {
            display: none !important;
            }

            .custom-video-controls {
            z-index: 2147483647;
            }

            video::-webkit-media-controls-enclosure {
            display: none !important;
            }

            video {
            width: 100%;
            height: auto;
            transform: rotateY(180deg);
            }

            video::-webkit-full-screen {
            transform: rotateY(180deg);
            }

                        /*style={{
              width: '100%',
              height: 'auto',
              transform: 'rotateY(180deg)',
              MozTransformOrigin: 'rotateY(180deg)',
              WebkitTransformOrigin: 'rotateY(180deg)',
              ['WebkitFullScreen' as any]:{
                transform: 'rotateY(180deg)',
                MozTransformOrigin: 'rotateY(180deg)',
                WebkitTransformOrigin: 'rotateY(180deg)',
              },
              ['fullscreen' as any]:{
                transform: 'rotateY(180deg)',
                MozTransformOrigin: 'rotateY(180deg)',
                WebkitTransformOrigin: 'rotateY(180deg)',
              }
            }}*/

- Using div element to transform , failed
  .videoPlayer {
    transform: rotateY(180deg);
  }

  .videoPlayer:fullscreen {
    transform: rotateY(180deg);
  }
  const handleFullscreen = () => {
    if (videoRef.current && videoRef.current.srcObject) {
      videoPlayer.current?.requestFullscreen();
    }
  };
  <div ref={videoPlayer} className="videoPlayer">
            <video
              ref={videoRef}
              autoPlay
            />
          </div>


- We loose connection between peers when we "start streaming" -> enabling camera -> stop streaming -> start streaming -> enable camera -> lost connection (fixed by removing mediaconnection.close())

- Issues with the XState
    - does not have typesafe for parallerl
    - it increases the json object
    - dont understand how to implement streams