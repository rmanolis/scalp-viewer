import { useEffect, useState } from 'react';


interface UseMediaOutput {
  mediaStream: MediaStream | null;
  setEnableCamera: React.Dispatch<React.SetStateAction<boolean>>;
  enableCamera: boolean;
}

export function useMedia(): UseMediaOutput {
  const [mediaStream, setMediaStream] = useState<MediaStream | null>(null);
  const [enableCamera, setEnableCamera] = useState(false);
  useEffect(() => {
    function enableStream() {
      const getUserMediaCallback =
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia;

      try {
        navigator.mediaDevices.getUserMedia({
          audio: false,
          video: true,
        })
          .then(stream => {
            setMediaStream(stream);
          })
          .catch(err => {
            console.error(err);
          });
      } catch (err) {
        console.error(err)
        getUserMediaCallback(
          {
            audio: false,
            video: true,
          },
          (stream: MediaStream) => {
            setMediaStream(stream);
          },
          (err: any) => {
            console.error(err);
          },
        );
      }
    }

    if (!mediaStream && enableCamera) {
      enableStream();
    } else if (mediaStream && !enableCamera) {
      mediaStream.getTracks().forEach(mst => mst.stop());
      setMediaStream(null);
    }
  }, [mediaStream, enableCamera]);

  return { mediaStream, setEnableCamera, enableCamera };
}
