const download = (blob: Blob | null) => {
  let a = document.createElement('a');
  a.href = URL.createObjectURL(blob);
  const now = new Date();
  const formNow = `${now.getFullYear()}-${now.getMonth() +
    1}-${now.getDate()}-${now.getHours()}-${now.getMinutes()}-${now.getSeconds()}`;
  a.download = `ScalpViewerPhoto-${formNow}.jpg`;
  document.body.appendChild(a);
  a.click();
};

export const takePhoto = (vid: HTMLVideoElement) => {
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');
  canvas.width = vid.videoWidth;
  canvas.height = vid.videoHeight;
  if (ctx) {
    // mirror effect
    ctx.translate(canvas.width, 0);
    ctx.scale(-1, 1);
    
    ctx.drawImage(vid, 0, 0);
    canvas.toBlob(download, 'image/jpeg');
  }
};
