import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
/// <reference types="webrtc" />
import 'webrtc-adapter';


ReactDOM.render(
    <App />,
  document.getElementById('root'),
);
