import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Home } from './pages/Home'
import { Stream } from './pages/Stream/Stream';
import { Guide } from './pages/Guide';

export const App = () => {
  return (
    <Router>
      <Switch>
        <Route  exact path="/">
          <Home />
        </Route>
        <Route  exact path="/guide">
          <Guide />
        </Route>
        <Route  exact path="/stream">
          <Stream />
        </Route>
        <Route  exact path="/stream/:peerId">
          <Stream />
        </Route>
      </Switch>
    </Router>
  );
};
