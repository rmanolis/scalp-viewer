import React from 'react'
import Typography from '@material-ui/core/Typography';
import LoopIcon from '@material-ui/icons/Loop';
import CachedIcon from '@material-ui/icons/Cached';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import QRCode from 'qrcode.react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
  },
}));
interface WaitingPropsI  {
    id: string
}
export const Waiting = (props:WaitingPropsI) => {
  const classes = useStyles();

  return (
    <Grid container spacing={3} justify="center">
      <Grid item xs={12}>
        <Typography variant="h5" align="center" color="primary" paragraph>
          Use your mobile's camera over the QR Code to continue
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Typography
          variant="body1"
          align="center"
          color="textSecondary"
          paragraph
        >
          <CachedIcon /> Waiting for the mobile to connect... <LoopIcon />
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Paper elevation={0} className={classes.paper}>
          <QRCode
            size={250}
            value={'https://' + window.location.host + '/stream/' + props.id}
          /> 
        </Paper>
      </Grid>
    </Grid>
  );
};
