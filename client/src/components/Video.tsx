import React, { useRef, useEffect, useCallback } from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import { takePhoto } from '../utils/takePhoto';
import './video.css';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
  },
}));
interface VideoPropsI {
  videoRef: React.MutableRefObject<HTMLVideoElement | null>;
  stopStreaming: () => void;
}

export const Video = (props: VideoPropsI) => {
  const classes = useStyles();
  const { videoRef, stopStreaming } = props;
  const playerRef = useRef<HTMLCanvasElement>(null);

  const drawCanvasVideoFrame = useCallback(() => {
    if (
      !playerRef.current ||
      !videoRef.current ||
      !videoRef.current.srcObject
    ) {
      console.log('a reference is null');
      return;
    }
    if (playerRef.current && videoRef.current && videoRef.current.srcObject) {
      const { videoHeight, videoWidth } = videoRef.current;
      playerRef.current.height = videoHeight
      playerRef.current.width = videoWidth
      const canvas = playerRef.current.getContext('2d');
      if (canvas) {
        // mirror effect
        canvas.translate(videoWidth, 0);
        canvas.scale(-1, 1);
        
        canvas.drawImage(videoRef.current, 0, 0);
        
        requestAnimationFrame(() => {
          drawCanvasVideoFrame();
        });
      } else {
        console.log('canvas was null');
        return;
      }
    }
  }, [playerRef, videoRef]);
  useEffect(() => {
    if (videoRef && videoRef.current) {
      videoRef.current.addEventListener(
        'play',
        () => {
          drawCanvasVideoFrame();
        },
        false,
      );
    }
  }, [videoRef, drawCanvasVideoFrame]);
  const handleFullscreen = () => {
    if (videoRef.current && videoRef.current.srcObject) {
      // videoRef.current.requestFullscreen();
      playerRef.current?.requestFullscreen();
    }
  };

  const handleTakePhoto = () => {
    if (videoRef.current && videoRef.current.srcObject) {
      takePhoto(videoRef.current);
    }
  };

  return (
    <Grid container spacing={3} justify="center">
      <Grid item xs={12}>
        <Grid container spacing={3} justify="center">
          <Grid item>
            <Button
              onClick={stopStreaming}
              color="secondary"
              variant="contained"
            >
              Stop
            </Button>
          </Grid>
          <Grid item>
            <Button onClick={handleFullscreen} variant="contained">
              <FullscreenIcon />
            </Button>
          </Grid>
          <Grid item>
            <Button onClick={handleTakePhoto} variant="contained">
              <PhotoCameraIcon />
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Paper elevation={0} className={classes.paper}>
          <canvas className="videoPlayer" ref={playerRef} />
        </Paper>
      </Grid>
    </Grid>
  );
};
