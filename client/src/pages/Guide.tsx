import React from 'react';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Img from 'react-image';
import { Layout } from '../components/Layout';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.primary,
  },
  text: {
    textAlign: 'initial',
    fontSize: 18,
  },
}));

export const Guide = () => {
  const classes = useStyles();

  const height = 600;
  const width = 500;
  return (
    <Layout>
      <Container maxWidth="md">
        <Grid justify="center" spacing={3} container>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.text}
                paragraph
                variant="body1"
                component="p"
                color="textPrimary"
                gutterBottom
              >
                ScalpViewer is a web app to help you see where the mirror can
                not reach. It connects two devices using P2P technology to
                stream video camera from one end to the other.
              </Typography>
              <Img src="/img/1.jpg" height={height} width={width} />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.text}
                paragraph
                variant="body1"
                component="p"
                color="textPrimary"
                gutterBottom
              >
                For this to work, you need two devices with a camera. From one
                of the devices press, 'READY' from the home page to create a QR
                code that the second device has to scan.
              </Typography>
              <Img src="/img/2.jpg" height={height} width={width} />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.text}
                paragraph
                variant="body1"
                component="p"
                color="textPrimary"
                gutterBottom
              >
                The QR code points to a link where the second device has to open
                from a browser. The link points to the same website but with a
                different URL parameter, for example,
                https://scalpviewer.com/stream/123e4567-e89b-12d3-a456-426655440000.
                The URL parameter contains a UUID that is common between the two
                devices, for the server to recognize these two devices as the
                same person.
              </Typography>
              <Img src="/img/3.jpg" height={height} width={width} />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.text}
                paragraph
                variant="body1"
                component="p"
                color="textPrimary"
                gutterBottom
              >
                When the browser opens the link, it shows a button "Connect"
                that you have to press to continue. This button opens a
                peer-to-peer connection between the devices.
              </Typography>
              <Img src="/img/4.jpg" height={height} width={width} />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.text}
                paragraph
                variant="body1"
                component="p"
                color="textPrimary"
                gutterBottom
              >
                After the connection, a button "Stream from here" shows up on
                both devices. Please press this button from the device that you
                want to open its video camera and stream from there.
              </Typography>
              <Img src="/img/5.jpg" height={height} width={width} />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.text}
                paragraph
                variant="body1"
                component="p"
                color="textPrimary"
                gutterBottom
              >
                Also, by pressing the button, the browser requests for approval
                the use of the video camera from the website.
              </Typography>
              <Img src="/img/6.jpg" height={height} width={width} />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.text}
                paragraph
                variant="body1"
                component="p"
                color="textPrimary"
                gutterBottom
              >
                When the streaming starts, three buttons and a video player
                shows up on both devices. The video player shows images from the
                device that you selected.
              </Typography>
              <Img src="/img/7.jpg" height={height} width={width} />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.text}
                paragraph
                variant="body1"
                component="p"
                color="textPrimary"
                gutterBottom
              >
                The three buttons over the video player are "Stop", fullscreen
                icon and camera icon.
                <ul>
                  <li>
                    The button "Stop", stops the streaming and shows the button
                    "Stream from here" on both devices.
                  </li>
                  <li>
                    The button with the fullscreen icon enables the fullscreen
                    mode locally.
                  </li>
                  <li>
                    The button with the camera icon takes a screenshot from the
                    video and saves it on the device.
                  </li>
                </ul>
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.text}
                paragraph
                variant="body1"
                component="p"
                color="error"
                gutterBottom
              >
                If any of the buttons does not work, then close the page and
                start the process from the beginning.
              </Typography>
              <Typography
                className={classes.text}
                paragraph
                variant="body1"
                component="p"
                color="error"
                gutterBottom
              >
                If you lose the connection between the devices, then the first
                device goes back to the page with the QR code and the second
                device goes back to the page with the "Connect" button.
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </Layout>
  );
};
