import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import { Layout } from '../components/Layout';

export const Home = withRouter(props => {
  const handleReady = () => {
    props.history.push('/stream');
  };

  const handleGuide = () => {
    props.history.push('/guide');
  };
  return (
    <Layout>
      <Container maxWidth="sm">
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="textPrimary"
          gutterBottom
        >
          You can see what the mirrors can not reach.
        </Typography>
        <Typography
          component="h3"
          variant="h4"
          align="center"
          color="textSecondary"
          gutterBottom
        >
          (alpha version)
        </Typography>
        <Typography variant="h5" align="center" color="textSecondary" paragraph>
          ScalpViewer is a web app to help you see your whole body using two devices with a camera.
          It connects the two devices using P2P technology to stream video camera
          from one end to the other.
        </Typography>
        <div>
          <Grid container spacing={2} justify="center">
            <Grid item>
              <Button variant="contained" onClick={handleReady} color="primary">
                Ready
              </Button>
            </Grid>
            <Grid item>
              <Button variant="outlined" onClick={handleGuide} color="primary">
                Guide
              </Button>
            </Grid>
          </Grid>
        </div>
      </Container>
    </Layout>
  );
});
