import Peer from 'peerjs';

export interface PeerStateSchema {
  states: {
    idle: {};
    serverConnected: {};
    peerConnected: {};
    streaming: {};
    accepting: {};
  };
}
export type InitializePeerEvent = { type: 'INITIALIZE_PEER'; peer: Peer }
export type OpenLocalConenctionEvent = { type: 'OPEN_LOCAL_CONNECTION' }
export type NewRemoteConnectionEvent = { type: 'NEW_REMOTE_CONNECTION'; remoteConnection: Peer.DataConnection }
export type CheckConnectionEvent = { type: 'CHECK_CONNECTION' }
export type ResetConnectionEvent = { type: 'RESET_CONNECTION' }
export type StartStreamingEvent = { type: 'START_STREAMING' }
export type AcceptStreamingEvent = { type: 'ACCEPT_STREAMING' }
export type StopStreamingEvent ={ type: 'STOP_STREAMING' }
export type PeerEvent =
  | InitializePeerEvent
  | OpenLocalConenctionEvent
  | NewRemoteConnectionEvent
  | CheckConnectionEvent
  | ResetConnectionEvent
  | StartStreamingEvent
  | AcceptStreamingEvent
  | StopStreamingEvent;

export interface PeerContext {
  remoteConnection: Peer.DataConnection | null;
  isConnectionOpened: boolean;
  peer: Peer | null;
}


export enum ActionStates {
  START_STREAMING = 'START_STREAMING',
  ACCEPT_STREAMING = 'ACCEPT_STREAMING',
  STOP = 'STOP',
  REQUEST_CONNECTION = 'REQUEST_CONNECTION',
}