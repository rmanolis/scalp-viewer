import { assign } from 'xstate';
import {
  InitializePeerEvent,
  PeerContext,
  OpenLocalConenctionEvent,
  NewRemoteConnectionEvent,
} from './types';

const isPeerDefined = (ctx: PeerContext) => {
  return !ctx.peer;
};
export const from_idle_to_serverConnected = {
  idle: {
    on: {
      INITIALIZE_PEER: [
        {
          target: 'serverConnected',
          cond: isPeerDefined,
          actions: assign<PeerContext, InitializePeerEvent>({
            peer: (ctx, event) => event.peer,
          }),
        },
        { target: 'idle' },
      ],
    },
  },
};

export const from_serverConnected_to_peerConnected = {
  serverConnected: {
    on: {
      OPEN_LOCAL_CONNECTION: {
        actions: assign<PeerContext, OpenLocalConenctionEvent>({
          isConnectionOpened: (context, event) => true,
        }),
      },
      NEW_REMOTE_CONNECTION: {
        actions: assign<PeerContext, NewRemoteConnectionEvent>({
          remoteConnection: (context, event) => event.remoteConnection,
        }),
      },
      CHECK_CONNECTION: [
        { target: 'peerConnected', cond: 'isConnected' },
        { target: 'serverConnected' },
      ],
    },
  },
};

export const from_peerConnected_to_streaming_or_accepting = {
  peerConnected: {
    on: {
      START_STREAMING: 'streaming',
      ACCEPT_STREAMING: 'accepting',
      RESET_CONNECTION: {
        target: 'serverConnected',
        actions: ['resetConnection'],
      },
    },
  },
};
export const from_streaming_to_peerConnected_or_serverConnected = {
  streaming: {
    on: {
      STOP_STREAMING: 'peerConnected',
      RESET_CONNECTION: {
        target: 'serverConnected',
        actions: ['resetConnection'],
      },
    },
  },
};

export const from_accepting_to_peerConnected_or_serverConnected = {
  accepting: {
    on: {
      STOP_STREAMING: 'peerConnected',
      RESET_CONNECTION: {
        target: 'serverConnected',
        actions: ['resetConnection'],
      },
    },
  },
};
