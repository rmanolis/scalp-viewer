import { Machine } from 'xstate';
import { PeerContext, PeerStateSchema, PeerEvent } from './types';
import * as fsm from './machineTransitions';
import * as actions from './actions';

const isConnected = (ctx: PeerContext) => {
  const res = ctx.remoteConnection !== null && ctx.isConnectionOpened === true;
  console.log('isConnected ', res);
  return res;
};

export const peerMachine = Machine<PeerContext, PeerStateSchema, PeerEvent>(
  {
    key: 'peer',
    initial: 'idle',
    context: {
      isConnectionOpened: false,
      remoteConnection: null,
      peer: null,
    },
    states: {
      ...fsm.from_idle_to_serverConnected,
      ...fsm.from_serverConnected_to_peerConnected,
      ...fsm.from_peerConnected_to_streaming_or_accepting,
      ...fsm.from_streaming_to_peerConnected_or_serverConnected,
      ...fsm.from_accepting_to_peerConnected_or_serverConnected,
    },
  },
  {
    guards: {
      isConnected,
    },
    actions,
  },
);
