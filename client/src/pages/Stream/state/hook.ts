import { PeerContext, PeerEvent, ActionStates } from './types';
import { State } from 'xstate';
import { useMachine } from '@xstate/react';
import { peerMachine } from './machine';
import { useMedia } from '../../../utils/useMedia';
import { useState, useEffect, useCallback, useMemo } from 'react';
import Peer from 'peerjs';
import { Config } from '../../../config';

interface UsePeerMachineOutput {
  state: State<PeerContext, PeerEvent, any, any>;
  actions: {
    createNewConnection: () => void;
    startStreaming: () => void;
    stopStreaming: () => void;
  };
  streams: {
    mediaStream: MediaStream | null;
    remoteStream: MediaStream | null;
  };
}

export const usePeerMachine = (
  myPeerId: string,
  remotePeerId: string,
): UsePeerMachineOutput => {
  const [state, send] = useMachine(peerMachine);
  const { mediaStream, setEnableCamera, enableCamera } = useMedia();
  const [remoteStream, setRemoteStream] = useState<MediaStream | null>(null);
  const [
    mediaConnection,
    setMediaConnection,
  ] = useState<Peer.MediaConnection | null>(null);

  // connect with the server
  // It creates a PeerJs instance on the start and only once
  useEffect(() => {
    connectServer(myPeerId, send);
  }, [myPeerId, send]);

  const { peer, isConnectionOpened } = state.context;

  const createNewConnection = useCallback(() => {
    if (peer) {
      createNewPeerConnection(peer, remotePeerId, isConnectionOpened, send);
    }
  }, [isConnectionOpened, peer, send, remotePeerId]);

  // It initializes the connection between the peers, when the first connection is open.
  const onOpenConnection = useCallback(() => {
    if (peer) {
      console.log('Opening connection with ', remotePeerId);
      if (!isConnectionOpened) {
        send({ type: 'OPEN_LOCAL_CONNECTION' });
        send({ type: 'CHECK_CONNECTION' });
      }
    }
  }, [peer, isConnectionOpened, send, remotePeerId]);

  // The remote peer commands us on what to do
  const onData = useCallback(
    (data: string) => {
      if (peer) {
        onDataConnection(peer, remotePeerId, send)(data);
      }
    },
    [peer, send, remotePeerId],
  );

  const onCloseConnection = useCallback(() => {
    send({ type: 'RESET_CONNECTION' });
  }, [send]);

  const onErrorConnection = useCallback(
    (err: any) => {
      console.error('Error connection: ', err);
      send({ type: 'RESET_CONNECTION' });
    },
    [send],
  );

  useEffect(() => {
    if (peer) {
      peer.on('connection', (conn: Peer.DataConnection) => {
        conn.on('open', onOpenConnection);
        conn.on('data', onData);
        conn.on('close', onCloseConnection);
        conn.on('error', onErrorConnection);
      });
      peer.on('disconnected', () => {
        console.log('disconnected ');
      });
    }
  }, [peer, onData, onOpenConnection, onCloseConnection, onErrorConnection]);

  const onErrorCall = (err: any) => {
    console.error('Error call: ', err);
  };

  // Effect for the remote stream
  useEffect(() => {
    if (peer) {
      peer.on('call', call => {
        console.log('remote call');
        call.answer();
        call.on('stream', remoteStream => {
          console.log('remote stream started');
          setRemoteStream(remoteStream);
        });
        call.on('close', onCloseConnection);
        call.on('error', onErrorCall);
      });
    }
  }, [peer, onCloseConnection]);

  // Effect for the media stream
  // After enabling camera we want to transmit our video camera to the remote peer
  useEffect(() => {
    if (peer) {
      if (mediaStream) {
        const peerStream = peer.call(remotePeerId, mediaStream);
        setMediaConnection(peerStream);
      }
    }
  }, [peer, mediaStream, remotePeerId]);

  const startStreaming = useCallback(() => {
    if (state.context.remoteConnection) {
      send({ type: 'START_STREAMING' });
      state.context.remoteConnection.send(ActionStates.ACCEPT_STREAMING);
    }
  }, [send, state.context.remoteConnection]);

  const stopStreaming = useCallback(() => {
    if (state.context.remoteConnection) {
      send({ type: 'STOP_STREAMING' });
      state.context.remoteConnection.send(ActionStates.STOP);
    }
  }, [send, state.context.remoteConnection]);

  useEffect(() => {
    if (peer) {
      switch (state.value.toString()) {
        case 'serverConnected':
          if (enableCamera) {
            setEnableCamera(false);
            if (mediaConnection) {
              //mediaConnection.close();
              setMediaConnection(null);
            }
          }
          break;
        case 'peerConnected':
          if (enableCamera) {
            setEnableCamera(false);
            if (mediaConnection) {
              //mediaConnection.close();
              setMediaConnection(null);
            }
          }
          if (remoteStream) {
            setRemoteStream(null);
          }
          break;
        case 'streaming':
          if (!enableCamera) {
            setEnableCamera(true);
          }
          break;
        case 'accepting':
          break;
        default:
      }
    }
  }, [
    peer,
    state.value,
    enableCamera,
    setRemoteStream,
    setEnableCamera,
    remoteStream,
    mediaStream,
    mediaConnection,
    remotePeerId,
  ]);

  useMemo(() => {
    console.log('Machine state: ', state.value);
    console.log('Machine context: ', state.context);
  }, [state]);

  return {
    state,
    actions: { createNewConnection, startStreaming, stopStreaming },
    streams: { mediaStream, remoteStream },
  };
};

const connectServer = (myPeerId: string, send: any) => {
  const { host, port, path, key } = Config.peerjs;
  const pr = new Peer(myPeerId, { host, port, path, key });
  console.log('Creating peer');
  pr.on('open', () => {
    console.log('Initializing peer for ', myPeerId);
    send({ type: 'INITIALIZE_PEER', peer: pr });
  });
  return () => {
    pr.disconnect();
  };
};

const createNewPeerConnection = (
  peer: Peer,
  remotePeerId: string,
  isConnectionOpened: boolean,
  send: any,
) => {
  const newConn = peer.connect(remotePeerId);
  if (newConn === undefined) {
    return;
  }
  send({ type: 'NEW_REMOTE_CONNECTION', remoteConnection: newConn });
  send({ type: 'CHECK_CONNECTION' });
  if (!isConnectionOpened) {
    setTimeout(() => {
      newConn.send(ActionStates.REQUEST_CONNECTION);
    }, 1000);
  }
};

const onDataConnection = (peer: Peer, remotePeerId: string, send: any) => {
  return (data: string) => {
    switch (data) {
      case ActionStates.REQUEST_CONNECTION:
        const newConn = peer.connect(remotePeerId);
        send({ type: 'NEW_REMOTE_CONNECTION', remoteConnection: newConn });
        send({ type: 'CHECK_CONNECTION' });

        break;
      case ActionStates.ACCEPT_STREAMING:
        send({ type: 'ACCEPT_STREAMING' });
        break;
      case ActionStates.START_STREAMING:
        send({ type: 'START_STREAMING' });
        break;
      case ActionStates.STOP:
        send({ type: 'STOP_STREAMING' });
        break;
      default:
    }
    console.log('Received message: ', data);
  };
};
