import { assign } from 'xstate';
import { PeerContext, PeerEvent } from './types';

export const resetConnection = assign<PeerContext, PeerEvent>({
  remoteConnection: (context, event) => {
    if (context.remoteConnection) context.remoteConnection.close();
    return null;
  },
  isConnectionOpened: (context, event) => false,
});
