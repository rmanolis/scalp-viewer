import React, { useEffect, useRef, useState, useMemo } from 'react';
import { useParams } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import * as uuid from 'uuid';
import { Layout } from '../../components/Layout';
import { Waiting } from '../../components/Waiting';
import { usePeerMachine } from './state/hook';
import { Video } from '../../components/Video';


export const Stream = () => {
  const [id, setId] = useState('');
  const { peerId } = useParams();
  const isLaptop = peerId ? false : true;
  useMemo(() => {
    if (!peerId) {
      setId(uuid.v4());
    } else {
      setId(peerId);
    }
  }, [peerId]);

  const myId = isLaptop ? `${id}-laptop` : `${id}-mobile`;
  const remoteId = isLaptop ? `${id}-mobile` : `${id}-laptop`;

  const { state, actions, streams } = usePeerMachine(myId, remoteId);
  const videoElem = document.createElement('video')
  const videoRef = useRef<HTMLVideoElement | null>(videoElem);
  const { mediaStream, remoteStream } = streams;

  useEffect(() => {
    switch (state.value.toString()) {
      case 'streaming':
        if (mediaStream && videoRef.current && !videoRef.current!.srcObject) {
          videoRef.current.srcObject = mediaStream;
          videoRef.current.autoplay = true;
        }
        break;
      case 'accepting':
        if (remoteStream && videoRef.current && !videoRef.current!.srcObject) {
          videoRef.current.srcObject = remoteStream;
          videoRef.current.autoplay = true;
        }
        break;
      case 'peerConnected':
        if (videoRef.current && videoRef.current.srcObject) {
          videoRef.current.pause();
          videoRef.current.srcObject = null;
        }
        break;
      default:
    }
  }, [state.value, mediaStream, remoteStream, videoRef]);


  const isStreaming = state.matches('streaming') || state.matches('accepting');
  return (
    <Layout>
      <Container maxWidth="md">
        {state.matches('serverConnected') && isLaptop && !isStreaming && (
          <Waiting id={id} />
        )}
        {state.matches('serverConnected') && !isLaptop && (
          <Button onClick={actions.createNewConnection} color="primary" variant="contained">
            Connect
          </Button>
        )}
        {state.matches('peerConnected') && (
          <Button onClick={actions.startStreaming}  color="primary" variant="contained">
            Stream from here
          </Button>
        )}
        {isStreaming && (
            <Video videoRef={videoRef} stopStreaming={actions.stopStreaming} />
        )}
      </Container>
    </Layout>
  );
};
