import express from 'express';
import https from 'https';
import fs from 'fs';
import { ExpressPeerServer } from './expressServer';
import { enableOnClose } from './onClose';

function init() {
  if (!process.env.PORT) {
    console.error('PORT is missing');
    process.exit(1);
  } else if (isNaN(Number(process.env.PORT))) {
    console.error('PORT is not a number');
    process.exit(1);
  }

  if (!process.env.CERT_KEY) {
    console.error('CERT_KEY is missing');
    process.exit(1);
  }

  if (!process.env.CERT_CRT) {
    console.error('CERT_CRT is missing');
    process.exit(1);
  }
  if (!process.env.CERT_CA) {
    console.error('CERT_CA is missing');
    process.exit(1);
  }
  const certKey = process.env.CERT_KEY;
  const certCrt = process.env.CERT_CRT;
  const certCa = process.env.CERT_CA;
  const port = process.env.PORT;
  const app = express();

  const ssl = {
    cert: fs.readFileSync(certCrt).toString(),
    key: fs.readFileSync(certKey).toString(),
    ca: fs.readFileSync(certCa).toString(),
  };
  const httpsServer = https.createServer(ssl, app);

  httpsServer.listen(port, () => {
    console.info('Starting on:');
    console.info('PORT: ', port);
  });

  enableOnClose(httpsServer);
  const peerServer = ExpressPeerServer(httpsServer, {
    port: 9000,
    expire_timeout: 5000,
    alive_timeout: 60000,
    key: 'kajdaskdjlasdlashdl12312k1jl23h123gh1k2h3gigkap[owpio1',
    path: '/myapp',
    concurrent_limit: 5000,
    allow_discovery: false,
    proxied: false,
    cleanup_out_msgs: 1000,
    ssl: {
      key: '',
      cert: '',
    },
  });
  app.use('/api', peerServer);
  app.use(function(req, _res, next) {
    console.log('Time:', Date.now());
    console.log('Request: ', req.headers);
    next();
  });
}

init();
