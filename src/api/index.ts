import bodyParser from "body-parser";
import express from "express";
import { IConfig } from "../config";
import { IMessageHandler } from "../messageHandler";
import { IRealm } from "../models/realm";
import { AuthMiddleware } from "./middleware/auth";
import CallsApi from "./v1/calls";
import PublicApi from "./v1/public";

export const Api = ({ config, realm, messageHandler }: {
  config: IConfig;
  realm: IRealm;
  messageHandler: IMessageHandler;
}): express.Router => {
  const authMiddleware = new AuthMiddleware(config, realm);

  const app = express.Router();

  const jsonParser = bodyParser.json();

  app.get("/", (_, res) => {
    res.send({
      "name": "ScalpViewer",
      "description": "A server side element to broker connections between clients for the ScalpViewer.",
      "website": "https://scalpviewer.com/"
    });
  });

  app.get("/scalpviewers_number", (_, res: express.Response) => {
    const clientsIds = realm.getClientsIds();
    return res.send(clientsIds.length.toString())
  })

  app.use("/:key", PublicApi({ config, realm }));
  app.use("/:key/:id/:token", authMiddleware.handle, jsonParser, CallsApi({ realm, messageHandler }));

  return app;
};
