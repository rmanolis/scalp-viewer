FROM node:alpine AS prod
ENV NODE_ENV=production
ENV PORT=9000
ENV CORS=scalpviewer.com
LABEL version=1
EXPOSE $PORT
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install --production && npm cache clean --force
COPY ./dist .
ENTRYPOINT ["node", "index.js"]

FROM node:alpine AS dev
ENV NODE_ENV=development
ENV PORT=9000
ENV CORS=192.168.1.101:3000
LABEL version=1
EXPOSE $PORT
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install --production && npm cache clean --force
COPY ./dist .
ENTRYPOINT ["node", "index.js"]