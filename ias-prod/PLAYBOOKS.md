# Read the stats
ansible-playbook -i dev_hosts.ini playbooks/print_stats.yml

# Update the UI
ansible-playbook -i dev_hosts.ini playbooks/update_ui.yml 