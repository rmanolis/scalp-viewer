variable "do_token" {}
variable "pub_key" {}
variable "pvt_key" {}
variable "ssh_fingerprint" {}

provider "digitalocean"{
    token = var.do_token
}


variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_zone_id" {}
variable "aws_dns" {}

variable "region" {
  default = "eu-central-1"
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region = var.region
}