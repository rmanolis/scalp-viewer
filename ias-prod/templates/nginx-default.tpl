upstream backend {
  ip_hash;
  {% for host in scalpviewer_servers %}
    server  {{ hostvars[host].inventory_hostname }}:9000;
  {% endfor %}
}
server {
    listen 9000 ssl;

    ssl_certificate /etc/letsencrypt/live/scalpviewer.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/scalpviewer.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    location / {
      proxy_pass https://backend;

      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
    }
}

server {
        listen 80 default_server;
        listen [::]:80 default_server;

        root /var/www/html;
        index index.html index.htm index.nginx-debian.html;

        server_name _;

        rewrite /stream /index.html;

}

server {

    root /var/www/html;
    index index.html index.htm index.nginx-debian.html;
    server_name scalpviewer.com; # managed by Certbot


    listen 127.0.0.1:80;
    location /nginx_status {
        stub_status;

        allow 127.0.0.1;
        deny all;
    }
    rewrite /stream /index.html;


    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/scalpviewer.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/scalpviewer.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
    if ($host = scalpviewer.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot
        
    listen 80 ;
    listen [::]:80 ;
    server_name scalpviewer.com;
    return 404; # managed by Certbot


}