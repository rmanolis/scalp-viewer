[scalpviewers]
${api_scalpviewers} ansible_ssh_private_key_file=${private_key}

[all]
${api_scalpviewers} ansible_ssh_private_key_file=${private_key}
${api_nginx} ansible_ssh_private_key_file=${private_key}

[nginx]
${api_nginx} ansible_ssh_private_key_file=${private_key}

