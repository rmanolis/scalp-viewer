resource "aws_route53_record" "www" {
  depends_on = [
    digitalocean_droplet.nginx_scalpviewer
  ]
  zone_id = var.aws_zone_id
  name    = var.aws_dns
  type    = "A"
  ttl     = "300"
  records = [digitalocean_droplet.nginx_scalpviewer.ipv4_address]
}


