resource "digitalocean_droplet" "scalpviewer" {
    count = 1
    image = "ubuntu-18-04-x64"
    name = "scalpviewer"
    region = "fra1"
    size = "s-1vcpu-1gb"
    private_networking = true
    ssh_keys = [
        var.ssh_fingerprint
    ]

    connection {
        user = "root"
        type = "ssh"
        host = self.ipv4_address
        private_key = file(var.pvt_key)
        timeout = "2m"
    }
}

output "scalp_viewer_ip_address" {
  value = digitalocean_droplet.scalpviewer.*.ipv4_address
}

