resource "digitalocean_droplet" "nginx_scalpviewer" {
    image = "ubuntu-18-04-x64"
    name = "nginx"
    region = "fra1"
    size = "s-1vcpu-1gb"
    private_networking = true
    ssh_keys = [
        var.ssh_fingerprint
    ]

    depends_on = [
        digitalocean_droplet.scalpviewer
    ]

    connection {
        user = "root"
        type = "ssh"
        host = self.ipv4_address
        private_key = file(var.pvt_key)
        timeout = "2m"
    }
}

output "nginx_ip_address" {
  value = digitalocean_droplet.nginx_scalpviewer.ipv4_address
} 
