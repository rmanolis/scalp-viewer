#!/bin/bash
IP=$1
CA_FOLDER=$2
cd $CA_FOLDER
echo '{}' | cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=cfssl.json \
    -hostname="localhost,127.0.0.1,$IP" - | cfssljson -bare server


