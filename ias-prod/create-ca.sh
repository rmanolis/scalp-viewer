#!/bin/bash
cd ca
if [ ! -f ca.pem ]; then
cfssl print-defaults csr | cfssl gencert -initca - | cfssljson -bare ca
fi