data "template_file" "dev_hosts" {
  template = "${file("${path.module}/templates/dev_hosts.cfg.tpl")}"
  depends_on = [
    digitalocean_droplet.scalpviewer,
    digitalocean_droplet.nginx_scalpviewer,
    aws_route53_record.www
  ]
  vars = {
    private_key = var.pvt_key
    api_scalpviewers = join("\n", digitalocean_droplet.scalpviewer.*.ipv4_address)
    api_nginx = digitalocean_droplet.nginx_scalpviewer.ipv4_address
  }
}

resource "null_resource" "dev-hosts" {
  triggers = {
    template_rendered = data.template_file.dev_hosts.rendered
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.dev_hosts.rendered}' > dev_hosts.ini"
  }
  provisioner "local-exec" {
    command = "sleep 60s; ansible-playbook -i dev_hosts.ini playbooks/install_node.yml -v"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i dev_hosts.ini playbooks/cert_scalpviewer.yml -v"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i dev_hosts.ini playbooks/setup_scalpviewer.yml -v"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i dev_hosts.ini playbooks/setup_lb.yml -v"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i dev_hosts.ini playbooks/increase_file_limits.yml -v"
  }
}